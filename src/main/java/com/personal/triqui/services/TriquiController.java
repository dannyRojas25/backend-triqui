package com.personal.triqui.services;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("triqui")
public class TriquiController {
	
	@GetMapping(value = "obtenerInicioMatriz", produces = MediaType.APPLICATION_JSON_VALUE)
	public String getLastMatrix() {
		return this.iniciarTriquiMatriz();
	}
	
	public String matriz [][] = new String [3][3];
	@GetMapping(value = "iniciar", produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin
	public String[][] iniciarTriqui() {	
		matriz[0][0] = "-";
		matriz[0][1] = "-";
		matriz[0][2] = "-";
		matriz[1][0] = "-";
		matriz[1][1] = "-";
		matriz[1][2] = "-";
		matriz[2][0] = "-";
		matriz[2][1] = "-";
		matriz[2][2] = "-";
	
		return matriz;
	}
	
	private String iniciarTriquiMatriz() {
		String reiniciarMAtriz = "";
		for (int i = 0; i < this.matriz.length; i++) {
			for (int j = 0; j < this.matriz[i].length; j++) {
				reiniciarMAtriz += this.matriz[i][j] + ",";
			}
		}
		return (reiniciarMAtriz.substring(0, reiniciarMAtriz.length() - 1));
	}
	
	@GetMapping(value = "comprobar", produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin
	public String probarSiHayTriqui(@RequestParam int x, @RequestParam int y, @RequestParam boolean raya) {
		if (raya) {
			this.matriz[x][y] = "O";
		} else {
			this.matriz[x][y] = "X";
		}
		return this.hayTriqui(raya);
	}
	
	private String hayTriqui(boolean raya) {
		if (raya) {
			if (this.matriz[0][0].equals("O") 
					&& this.matriz[0][1].equals("O") 
					&& this.matriz[0][2].equals("O")) {
				return "Gano-O";
			}
			if (this.matriz[1][0].equals("O") 
					&& this.matriz[1][1].equals("O") 
					&& this.matriz[1][2].equals("O")) {
				return "Gano-O";
			}
			if (this.matriz[2][0].equals("O") 
					&& this.matriz[2][1].equals("O") 
					&& this.matriz[2][2].equals("O")) {
				return "Gano-O";
			}
			if (this.matriz[0][0].equals("O") 
					&& this.matriz[1][0].equals("O") 
					&& this.matriz[2][0].equals("O")) {
				return "Gano-O";
			}
			if (this.matriz[0][1].equals("O") 
					&& this.matriz[1][1].equals("O") 
					&& this.matriz[2][1].equals("O")) {
				return "Gano-O";
			}
			if (this.matriz[0][2].equals("O") 
					&& this.matriz[1][2].equals("O") 
					&& this.matriz[2][2].equals("O")) {
				return "Gano-O";
			}
			if (this.matriz[0][0].equals("O") 
					&& this.matriz[1][1].equals("O") 
					&& this.matriz[2][2].equals("O")) {
				return "Gano-O";
			}
			if (this.matriz[2][0].equals("O") 
					&& this.matriz[1][1].equals("O") 
					&& this.matriz[0][2].equals("O")) {
				return "Gano-O";
			}
		} else {
			if (this.matriz[0][0].equals("X") 
					&& this.matriz[0][1].equals("X") 
					&& this.matriz[0][2].equals("X")) {
				return "Gano-X";
			}
			if (this.matriz[1][0].equals("X") 
					&& this.matriz[1][1].equals("X") 
					&& this.matriz[1][2].equals("X")) {
				return "Gano-X";
			}
			if (this.matriz[2][0].equals("X") 
					&& this.matriz[2][1].equals("X") 
					&& this.matriz[2][2].equals("X")) {
				return "Gano-X";
			}
			if (this.matriz[0][0].equals("X") 
					&& this.matriz[1][0].equals("X") 
					&& this.matriz[2][0].equals("X")) {
				return "Gano-X";
			}
			if (this.matriz[0][1].equals("X") 
					&& this.matriz[1][1].equals("X") 
					&& this.matriz[2][1].equals("X")) {
				return "Gano-X";
			}
			if (this.matriz[0][2].equals("X") 
					&& this.matriz[1][2].equals("X") 
					&& this.matriz[2][2].equals("X")) {
				return "Gano-X";
			}
			if (this.matriz[0][0].equals("X") 
					&& this.matriz[1][1].equals("X") 
					&& this.matriz[2][2].equals("X")) {
				return "Gano-X";
			}
			if (this.matriz[2][0].equals("X") 
					&& this.matriz[1][1].equals("X") 
					&& this.matriz[0][2].equals("X")) {
				return "Gano-X";
			}
		}
		return iniciarTriquiMatriz();
	}

}
